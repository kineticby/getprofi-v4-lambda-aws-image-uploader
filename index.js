var aws = require('aws-sdk');
var sharp = require('sharp');
var fs = require('fs');
var path = require('path');
var _ = require('lodash');
var mime = require('mime');
var extractParams = require('./extractParams.js');

/*aws.config.update({
  "accessKeyId": process.env.AWS_ACCESS_KEY_ID,
  "secretAccessKey": process.env.AWS_SECRET_ACCESS_KEY,
  "region": process.env.AWS_REGION
});*/

module.exports = function(wrappedImage, metadata, sizes) {
  //var s3 = new aws.S3();
  var extractedParams = extractParams(metadata.width, metadata.height);
  var uploadingOptions = {
    Bucket: process.env.AWS_BUCKET,
    ContentType: mime.lookup(metadata.format),
    ContentEncoding: 'utf-8',
    CacheControl: 'max-age=3600, must-revalidate',
    ACL: 'public-read'
  };
  var pipeline = sharp(wrappedImage)
    .extract(extractedParams)
    .quality(85);

  return [uploadingOptions, metadata, sizes];
};
