module.exports = function (width, height) {
  var size = Math.min(width, height);
  var extractParams = {
    width: size,
    height: size
  };

  if (height > width) {
    extractParams.top = Math.floor((height - width) / 2);
    extractParams.left = 0;
  } else if (width > height) {
    extractParams.left = Math.floor((width - height) / 2);
    extractParams.top = 0;
  } else {
    extractParams.left = 0;
    extractParams.top = 0;
  }

  return extractParams;
};
